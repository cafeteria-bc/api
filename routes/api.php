<?php

use App\Http\Controllers\BulkDepositController;
use App\Http\Controllers\DepositController;
use App\Http\Controllers\DepositExportController;
use App\Http\Controllers\PocketController;
use Illuminate\Support\Facades\Route;

Route::get('/v1/deposits', [DepositController::class, 'index']);
Route::post('/v1/deposits/bulk', [BulkDepositController::class, 'store']);
Route::get('/v1/deposits/export', [DepositExportController::class, 'index']);

Route::get('/v1/pockets', [PocketController::class, 'index']);
