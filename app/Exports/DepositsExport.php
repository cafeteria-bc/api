<?php

namespace App\Exports;

use App\Models\Deposit;
use App\Repositories\DepositRepository;
use Maatwebsite\Excel\Concerns\FromCollection;

class DepositsExport implements FromCollection
{
    public function __construct(private DepositRepository $deposits)
    {
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->deposits->getExportData();
    }
}
