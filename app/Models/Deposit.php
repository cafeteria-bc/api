<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Deposit extends Model
{
    use HasFactory;

    public function pocket()
    {
        return $this->belongsTo(Pocket::class);
    }
}
