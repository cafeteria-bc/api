<?php

namespace App\Services;

use App\Dtos\BulkDepositDto;
use App\Repositories\PocketRepository;
use Illuminate\Validation\ValidationException;

final class BulkDepositValidationService
{
    public function __construct(
        private int $yearlyCafeteriaLimit,
        private int $yearlyPocketLimit,
        private BulkDepositService $bulkDepositService,
        private PocketRepository $pockets
    ) {}

    /**
     * @throws ValidationException
     */
    public function validate(BulkDepositDto $bulkDeposit): void
    {
        $totalAmount = $this->bulkDepositService->getTotalAmount($bulkDeposit);
        if ($totalAmount > $this->yearlyCafeteriaLimit) {
            throw ValidationException::withMessages([
                'totalAmount' => [
                    "Total amount cannot be larger than $this->yearlyCafeteriaLimit"
                ]
            ]);
        }

        foreach ($bulkDeposit->getPockets() as $pocketId => $monthlyDeposits) {
            $totalAmount = $this->bulkDepositService
                ->getTotalAmountByMonthlyDeposits($monthlyDeposits);

            if ($totalAmount > $this->yearlyPocketLimit) {
                $pocket = $this->pockets->getById($pocketId);
                throw ValidationException::withMessages([
                    'totalAmount' => [
                        "Total amount for pocket $pocket->name cannot be larger than $this->yearlyPocketLimit"
                    ]
                ]);
            }
        }
    }
}
