<?php

namespace App\Services;

use App\Dtos\BulkDepositDto;
use App\Dtos\MonthlyDepositDto;
use App\Repositories\DepositRepository;
use App\Repositories\PocketRepository;
use Illuminate\Support\Facades\DB;

final class BulkDepositService
{
    public function __construct(
        private DepositRepository $deposits,
        private PocketRepository $pockets
    ) {}

    public function upsert(BulkDepositDto $bulkDeposit)
    {
        DB::transaction(function () use ($bulkDeposit) {
            foreach ($bulkDeposit->getPockets() as $pocketId => $monthlyDeposits) {
                $pocket = $this->pockets->getById($pocketId);
                $this->deposits->upsertAllForPocket($pocket, $monthlyDeposits);

                $totalAmount = $this->getTotalAmountByMonthlyDeposits($monthlyDeposits);
                $this->pockets->updateAmount($pocket, $totalAmount);
            }
        });
    }

    public function createBulkDepositDto(array $data): BulkDepositDto
    {
        $bulkDeposit = new BulkDepositDto();
        foreach ($data as $pocketId => $monthlyAmounts) {
            $bulkDeposit->openPocket($pocketId);
            foreach ($monthlyAmounts as $month => $amount) {
                if (empty($amount)) {
                    continue;
                }

                $bulkDeposit->addDeposit(
                    $pocketId,
                    new MonthlyDepositDto($month, $amount)
                );
            }
        }

        return $bulkDeposit;
    }

    public function getTotalAmount(BulkDepositDto $bulkDeposit): int
    {
        $totalAmount = 0;
        foreach ($bulkDeposit->getPockets() as $monthlyDeposits) {
            $totalAmount += $this->getTotalAmountByMonthlyDeposits($monthlyDeposits);
        }

        return $totalAmount;
    }

    /**
     * @param array{int: MonthlyDepositDto} $monthlyDeposits
     */
    public function getTotalAmountByMonthlyDeposits(
        array $monthlyDeposits
    ): int {
        $totalAmount = 0;
        foreach ($monthlyDeposits as $monthlyDeposit) {
            /** @var MonthlyDepositDto $monthlyDeposit */
            $totalAmount += $monthlyDeposit->getAmount();
        }

        return $totalAmount;
    }
}
