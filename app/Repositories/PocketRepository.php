<?php

namespace App\Repositories;

use App\Models\Pocket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class PocketRepository
{
    /**
     * @throws ModelNotFoundException
     */
    public function getById(string $id): Pocket
    {
        return Pocket::findOrFail($id);
    }

    public function updateAmount(Pocket $pocket, int $amount): Pocket
    {
        $pocket->amount = $amount;
        $pocket->save();

        return $pocket;
    }

    /**
     * @return Collection<Pocket>
     */
    public function getAll(): Collection
    {
        return Pocket::all();
    }
}
