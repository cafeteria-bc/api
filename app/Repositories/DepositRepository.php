<?php

namespace App\Repositories;

use App\Dtos\MonthlyDepositDto;
use App\Models\Deposit;
use App\Models\Pocket;
use Illuminate\Support\Collection;

final class DepositRepository
{
    /**
     * @param array{int: MonthlyDepositDto} $monthlyDeposits
     */
    public function upsertAllForPocket(Pocket $pocket, array $monthlyDeposits): void
    {
        foreach ($monthlyDeposits as $monthlyDeposit) {
            /** @var MonthlyDepositDto $monthlyDeposit */

            $deposit = Deposit::firstOrNew([
                'pocket_id' => $pocket->id,
                'month' => $monthlyDeposit->getMonth()
            ]);

            $deposit->amount = $monthlyDeposit->getAmount();
            $deposit->save();
        }
    }

    public function getExportData(): Collection
    {
        return Deposit::select(['pockets.name', 'deposits.month', 'deposits.amount',])
            ->leftJoin('pockets', 'deposits.pocket_id', '=', 'pockets.id')
            ->get();
    }

    /**
     * @return Collection<Deposit>
     */
    public function getAll(): Collection
    {
        return Deposit::orderBy('month')->get();
    }
}
