<?php

namespace App\Dtos;

final class BulkDepositDto
{
    /**
     * Pocket ID kulcs alatt tárolja a hónapokat és összegeket
     *
     * ```
     * '1' => [
     *   MonthylDepositDto
     * ]
     * ```
     * @var array{string: array{int: MonthlyDepositDto}}
     */
    private array $pockets;

    /**
     * @return array{string: array{int: MonthlyDepositDto}}
     */
    public function getPockets(): array
    {
        return $this->pockets;
    }

    public function openPocket(string $pocketId): void
    {
        if (!isset($this->pockets[$pocketId])) {
            // @phpstan-ignore-next-line
            $this->pockets[$pocketId] = [];
        }
    }

    public function addDeposit(
        string $pocketId,
        MonthlyDepositDto $monthlyDeposit
    ): void {
        $this->openPocket($pocketId);
        $this->pockets[$pocketId][] = $monthlyDeposit;
    }
}
