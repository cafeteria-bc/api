<?php

namespace App\Dtos;

final class MonthlyDepositDto
{
    /**
     * @param string $month Hónap '2021-01' formátumban
     */
    public function __construct(private string $month, private int $amount) {}

    public function getMonth(): string
    {
        return $this->month;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }
}
