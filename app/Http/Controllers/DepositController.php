<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepositResource;
use App\Repositories\DepositRepository;

class DepositController extends Controller
{
    public function __construct(private DepositRepository $deposits)
    {
    }

    public function index()
    {
        return DepositResource::collection($this->deposits->getAll());
    }
}
