<?php

namespace App\Http\Controllers;

use App\Http\Resources\PocketResource;
use App\Repositories\PocketRepository;

class PocketController extends Controller
{
    public function __construct(private PocketRepository $pockets)
    {
    }

    public function index()
    {
        return PocketResource::collection($this->pockets->getAll());
    }
}
