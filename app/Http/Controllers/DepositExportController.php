<?php

namespace App\Http\Controllers;

use App\Exports\DepositsExport;
use Maatwebsite\Excel\Facades\Excel;

class DepositExportController extends Controller
{
    public function __construct(private DepositsExport $depositsExport)
    {
    }

    public function index()
    {
        return Excel::download($this->depositsExport, 'deposits.csv');
    }
}
