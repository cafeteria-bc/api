<?php

namespace App\Http\Controllers;

use App\Services\BulkDepositService;
use App\Services\BulkDepositValidationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class BulkDepositController extends Controller
{
    public function __construct(
        private BulkDepositService $bulkDepositService,
        private BulkDepositValidationService $bulkDepositValidationService
    ) {}

    public function store(Request $request)
    {
        try {
            $bulkDepositDto = $this->bulkDepositService->createBulkDepositDto(
                $request->all()
            );

            $this->bulkDepositValidationService->validate($bulkDepositDto);
            $this->bulkDepositService->upsert($bulkDepositDto);

            return response()->json([], Response::HTTP_NO_CONTENT);
        } catch (ValidationException $ex) {
            return response()->json($ex->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
