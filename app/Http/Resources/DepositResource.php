<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepositResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'pocket' => new PocketResource($this->pocket),
            'month' => (string) $this->month,
            'amount' => (int) $this->amount
        ];
    }
}
