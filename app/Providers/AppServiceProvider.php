<?php

namespace App\Providers;

use App\Services\BulkDepositValidationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->app->when(BulkDepositValidationService::class)
            ->needs('$yearlyCafeteriaLimit')
            ->give(config('limits.yearly_cafeteria_limit'));

        $this->app->when(BulkDepositValidationService::class)
            ->needs('$yearlyPocketLimit')
            ->give(config('limits.yearly_pocket_limit'));
    }
}
