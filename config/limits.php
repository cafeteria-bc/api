<?php

return [
    'yearly_cafeteria_limit' => env('YEARLY_CAFETERIA_LIMIT'),
    'yearly_pocket_limit' => env('YEARLY_POCKET_LIMIT')
];
