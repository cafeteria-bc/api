<?php

namespace Database\Factories;

use App\Models\Pocket;
use Illuminate\Database\Eloquent\Factories\Factory;

class PocketFactory extends Factory
{
    protected $model = Pocket::class;

    public function definition()
    {
        return [
            'name' => $this->faker->words(2, true),
            'amount' => rand(0, 200000)
        ];
    }
}
