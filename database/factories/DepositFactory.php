<?php

namespace Database\Factories;

use App\Models\Deposit;
use App\Models\Pocket;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepositFactory extends Factory
{
    protected $model = Deposit::class;

    public function definition()
    {
        return [
            'pocket_id' => fn () => Pocket::factory()->create(),
            'amount' => rand(1000, 200000),
            'month' => '2021-' . rand(1, 12)
        ];
    }
}
