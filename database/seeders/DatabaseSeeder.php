<?php

namespace Database\Seeders;

use App\Models\Deposit;
use App\Models\Pocket;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $pockets = Pocket::factory()
            ->state(new Sequence(
                ['name' => 'Étkezés', 'amount' => 200000],
                ['name' => 'Utazás', 'amount' => 50000],
                ['name' => 'Ruha', 'amount' => 150000]
            ))
            ->count(3)
            ->create();

        // foreach ($pockets as $pocket) {
        //     foreach (range(1, 12) as $month) {
        //         Deposit::factory([
        //             'pocket_id' => $pocket,
        //             'amount' => $pocket->amount / 12,
        //             'month' => '2021-' . str_pad($month, 2, '0', STR_PAD_LEFT)
        //         ])
        //         ->create();
        //     }
        // }
    }
}
