<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTable extends Migration
{
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pocket_id');
            $table->integer('amount', false, true)->nullable(false);
            $table->string('month', 8)->nullable(false);
            $table->timestamps();

            $table->unique(['pocket_id', 'month']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
