<?php

namespace Tests;

use App\Models\Pocket;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function createPockets()
    {
        return Pocket::factory()
            ->state(new Sequence(
                ['name' => 'Étkezés', 'amount' => 0],
                ['name' => 'Utazás', 'amount' => 0],
                ['name' => 'Ruha', 'amount' => 0],
            ))
            ->count(3)
            ->create();
    }

    // -------- Helpers --------
    protected function assertDeposit(Pocket $pocket, int $amount, string $month)
    {
        $this->assertDatabaseHas('deposits', [
            'pocket_id' => $pocket->id,
            'amount' => $amount,
            'month' => $month
        ]);
    }
}
