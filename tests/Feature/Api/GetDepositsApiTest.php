<?php

namespace Tests\Feature\Api;

use App\Models\Deposit;
use App\Models\Pocket;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class GetDepositsApiTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_return_200()
    {
        $this->createPockets();
        $response = $this->json('GET', '/api/v1/deposits');

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function it_should_return_deposits_ordered_by_month()
    {
        Deposit::factory(['amount' => '1000', 'month' => '2021-11'])->create();
        Deposit::factory(['amount' => '1000', 'month' => '2021-01'])->create();
        Deposit::factory(['amount' => '2000', 'month' => '2021-02'])->create();
        $deposits = $this->json('GET', '/api/v1/deposits')->json('data');

        $this->assertEquals('2021-01', $deposits[0]['month']);
        $this->assertEquals('2021-02', $deposits[1]['month']);
        $this->assertEquals('2021-11', $deposits[2]['month']);
    }

    /** @test */
    public function it_should_return_pocket_with_deposit()
    {
        $pocket = Pocket::factory(['name' => 'Étkezés'])->create();
        Deposit::factory([
            'amount' => '1000',
            'month' => '2021-11',
            'pocket_id' => $pocket
        ])->create();

        $deposits = $this->json('GET', '/api/v1/deposits')->json('data');

        $this->assertEquals('Étkezés', $deposits[0]['pocket']['name']);
    }
}
