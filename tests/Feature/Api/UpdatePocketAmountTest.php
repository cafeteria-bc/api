<?php

namespace Tests\Feature\Api;

use App\Models\Pocket;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdatePocketAmountTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_update_a_pocket_amount()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
            ],
            $pockets[1]->id => [
                '2021-01' => 2000,
            ],
            $pockets[2]->id => [
                '2021-01' => 3000,
            ]
        ];

        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $this->assertPocketAmount($pockets[0], 1000);
        $this->assertPocketAmount($pockets[1], 2000);
        $this->assertPocketAmount($pockets[2], 3000);
    }

    /** @test */
    public function it_should_update_a_pocket_amount_with_multiple_deposits()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
                '2021-02' => 1000,
                '2021-03' => 1000,
                '2021-04' => 1000,
                '2021-05' => 1000,
                '2021-06' => 1000,
                '2021-07' => 1000,
                '2021-08' => 1000,
                '2021-09' => 1000,
                '2021-10' => 1000,
                '2021-11' => 1000,
                '2021-12' => 1000,
            ],
            $pockets[1]->id => [
                '2021-01' => 2000,
            ],
            $pockets[2]->id => [
                '2021-01' => 3000,
            ]
        ];

        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $this->assertPocketAmount($pockets[0], 12000);
    }

    // -------- Helpers --------
    public function assertPocketAmount(Pocket $pocket, int $amount)
    {
        $this->assertDatabaseHas('pockets', [
            'id' => $pocket->id,
            'amount' => $amount
        ]);
    }
}
