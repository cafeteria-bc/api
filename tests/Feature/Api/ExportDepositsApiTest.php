<?php

namespace Tests\Feature\Api;

use App\Exports\DepositsExport;
use App\Models\Deposit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Maatwebsite\Excel\Facades\Excel;

class ExportDepositsApiTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_download_deposits_csv()
    {
        Excel::fake();

        Deposit::factory(['amount' => 1000, 'month' => '2021-01'])->create();

        $this->json('GET', '/api/v1/deposits/export');

        Excel::assertDownloaded('deposits.csv', function(DepositsExport $export) {
            return $export->collection()->contains('month', '2021-01');
        });
    }
}
