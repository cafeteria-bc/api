<?php

namespace Tests\Feature\Api;

use App\Models\Pocket;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class StoreDepositsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_return_204()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 10000,
            ],
            $pockets[1]->id => [
                '2021-01' => 10000,
            ],
            $pockets[2]->id => [
                '2021-01' => 10000,
            ]
        ];

        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_NO_CONTENT);
    }

    /** @test */
    public function it_should_return_422_if_total_amount_above_limit()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 200000,
            ],
            $pockets[1]->id => [
                '2021-01' => 200000,
            ],
            $pockets[2]->id => [
                '2021-01' => 200000,
            ]
        ];

        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_should_return_422_if_one_pocket_above_limit()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 300000,
            ],
            $pockets[1]->id => [
                '2021-01' => 1000,
            ],
            $pockets[2]->id => [
                '2021-01' => 2000,
            ]
        ];

        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_should_store_deposits()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
            ],
            $pockets[1]->id => [
                '2021-02' => 2000,
            ],
            $pockets[2]->id => [
                '2021-03' => 3000,
            ]
        ];

        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $this->assertDeposit($pockets[0], 1000, '2021-01');
        $this->assertDeposit($pockets[1], 2000, '2021-02');
        $this->assertDeposit($pockets[2], 3000, '2021-03');
    }

    /** @test */
    public function it_should_store_multiple_deposits_for_a_pocket()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
                '2021-02' => 1000,
                '2021-03' => 1000,
                '2021-04' => 1000,
                '2021-05' => 1000,
                '2021-06' => 1000,
                '2021-07' => 1000,
                '2021-08' => 1000,
                '2021-09' => 1000,
                '2021-10' => 1000,
                '2021-11' => 1000,
                '2021-12' => 1000,
            ],
            $pockets[1]->id => [
                '2021-02' => 2000,
            ],
            $pockets[2]->id => [
                '2021-03' => 3000,
            ]
        ];

        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $this->assertDeposit($pockets[0], 1000, '2021-01');
        $this->assertDeposit($pockets[0], 1000, '2021-02');
        $this->assertDeposit($pockets[0], 1000, '2021-03');
        $this->assertDeposit($pockets[0], 1000, '2021-04');
        $this->assertDeposit($pockets[0], 1000, '2021-05');
        $this->assertDeposit($pockets[0], 1000, '2021-06');
        $this->assertDeposit($pockets[0], 1000, '2021-07');
        $this->assertDeposit($pockets[0], 1000, '2021-08');
        $this->assertDeposit($pockets[0], 1000, '2021-09');
        $this->assertDeposit($pockets[0], 1000, '2021-10');
        $this->assertDeposit($pockets[0], 1000, '2021-11');
        $this->assertDeposit($pockets[0], 1000, '2021-12');
    }

    /** @test */
    public function it_should_not_store_empty_values()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 0,
            ],
            $pockets[1]->id => [
                '2021-02' => '',
            ],
            $pockets[2]->id => [
                '2021-03' => null,
            ]
        ];

        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseCount('deposits', 0);
    }
}
