<?php

namespace Tests\Feature\Api;

use App\Models\Pocket;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class GetPocketsApiTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_return_200()
    {
        $this->createPockets();
        $response = $this->json('GET', '/api/v1/pockets');

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function it_should_return_all_pockets()
    {
        Pocket::factory(['name' => 'Étkezés'])->create();
        Pocket::factory(['name' => 'Utazás'])->create();
        $pockets = $this->json('GET', '/api/v1/pockets')->json('data');

        $names = collect($pockets)->pluck('name');
        $this->assertTrue($names->contains('Étkezés'));
        $this->assertTrue($names->contains('Utazás'));
        $this->assertCount(2, $names);
    }
}
