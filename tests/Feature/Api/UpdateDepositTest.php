<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;

class UpdateDepositTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_should_update_existing_deposits()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
            ],
            $pockets[1]->id => [
                '2021-02' => 1000,
            ],
            $pockets[2]->id => [
                '2021-03' => 1000,
            ]
        ];

        // Create
        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $data = [
            $pockets[0]->id => [
                '2021-01' => 2000,
            ],
            $pockets[1]->id => [
                '2021-02' => 3000,
            ],
            $pockets[2]->id => [
                '2021-03' => 4000,
            ]
        ];

        // Update
        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_NO_CONTENT);

        // Only updates, no new rows
        $this->assertDatabaseCount('deposits', 3);
        $this->assertDeposit($pockets[0], 2000, '2021-01');
        $this->assertDeposit($pockets[1], 3000, '2021-02');
        $this->assertDeposit($pockets[2], 4000, '2021-03');
    }

    /** @test */
    public function it_should_insert_new_deposits_and_update_existing_deposits()
    {
        $pockets = $this->createPockets();
        $data = [
            $pockets[0]->id => [
                '2021-01' => 1000,
            ],
            $pockets[1]->id => [
                '2021-02' => 1000,
            ],
            $pockets[2]->id => [
                '2021-03' => 1000,
            ]
        ];

        // Create
        $this->json('POST', '/api/v1/deposits/bulk', $data);

        $data = [
            $pockets[0]->id => [
                '2021-01' => 2000,
                '2021-02' => 10000
            ],
            $pockets[1]->id => [
                '2021-02' => 3000,
            ],
            $pockets[2]->id => [
                '2021-03' => 4000,
            ]
        ];

        // Update
        $response = $this->json('POST', '/api/v1/deposits/bulk', $data);
        $response->assertStatus(Response::HTTP_NO_CONTENT);

        // One insert, 3 updates
        $this->assertDatabaseCount('deposits', 4);
        $this->assertDeposit($pockets[0], 2000, '2021-01');
        $this->assertDeposit($pockets[0], 10000, '2021-02');
        $this->assertDeposit($pockets[1], 3000, '2021-02');
        $this->assertDeposit($pockets[2], 4000, '2021-03');
    }
}
